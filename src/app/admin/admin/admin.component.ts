import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { error } from 'selenium-webdriver';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  items : any;
  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.menu();
    this.checkLogin();
  }
  menu(){
    this.items=[
      {icon:'dashboard', name:'Dasboard', url:'/admin/dashboard'},
      {icon:'store', name:'Product', url:'/admin/produk'} 
    ]
  }

  checkLogin()
  {
    this.api.get('bookswithauth/status').subscribe(res=>{
      //is logged in
      return;
    },error=>{
      //not logged in
      this.router.navigate(['/login']);
    })
  }

  logout()
  {
    let conf=confirm('Keluar Aplikasi?');
    if(conf)
    {
      localStorage.removeItem('appToken');
      window.location.reload();
    }
  }
}
